
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Add Product</title>

 
    <!-- Bootstrap core CSS -->
<link href="https://getbootstrap.com/docs/4.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">
  </head>
<body>
<main class="container mt-5">
<h5><a href="index.php?action=add"> Add Procut</a><a href="/"> Product List</a></h5>

    <div class="row">
        <div class="col-md-8 mb-3">
           <h4>Add Product</h4>
           <form method="post" action="index.php?action=add" >
            <label >Product Name</label>
            <input type="text" name="name"  class="form-control">
           
            <?php $uniqid = mt_rand(1,1000); ?>
            <label for="sku">SKU</label>
            <input type="text" value="<?php echo "$uniqid" ?>" name="sku" id="sku" readonly class="form-control">

            <label for="price">Price ($)</label>
            <input type="text" name="price" id="price" class="form-control">

            <label for="size">Size (MB)</label>
            <input type="text" name="size" id="size" class="form-control">

            <label for="weight">weight (KG)</label>
            <input type="text" name="weight" id="weight" class="form-control">

            <label for="size_f">dimension  (Height x Width x Lenght)</label>
            <input type="text" name="size_f" id="size_f" class="form-control">

            <input type="submit" value="Add" class="btn btn-success mt-3">
                
           

           </form>
        </div>
    
   
 
    </div>


</main>
</body>
</html>