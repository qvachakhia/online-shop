<?php 

    function product_all($link)
    {
        //მოთხოვნა
        $query = "SELECT * FROM product ORDER BY id DESC";
        $result = mysqli_query($link, $query);
        
        if (!$result)
        {
            die(mysqli_error($link));
        }

        $n = mysqli_num_rows($result);
        $products = array();

        for ($i = 0; $i < $n; $i++)
        {
            $row = mysqli_fetch_assoc($result);
            $products[] = $row;
        }

        return $products;
    
    
    }

   

    function product_new($link , $name , $sku , $price , $size , $weight,$size_f)
{
    //reading
    $name =   trim($name);
    $sku =    trim($sku);
    $price =  trim($price);
    $size =   trim($size);
    $weight = trim($weight);
    $size_f = trim($size_f);

    //Check
    if ($name =='')
    {    
    return false;
    }
    $t = "INSERT INTO product (name,sku,price,size,weight,size_f) VALUES ('%s','%s','%s','%s','%s','%s')";

    $query = sprintf(
    $t,mysqli_real_escape_string($link,$name),
    mysqli_real_escape_string($link,$sku),
    mysqli_real_escape_string($link,$price),
    mysqli_real_escape_string($link,$size),
    mysqli_real_escape_string($link,$weight),
    mysqli_real_escape_string($link,$size_f));

    $result = mysqli_query($link,$query);

    if(!$result)
    {
        die(mysqli_error($link));
    }
    return true;
}


function product_delete($link,$id)
{
    $id = (int)$id;

    if ($id == 0)
    {
        return false;
    }

    $query = sprintf("DELETE FROM product WHERE id='%d' " , $id);
    $result = mysqli_query($link,$query);

    if (!$result)
    {
        die(mysqli_error($link));
    }
    return mysqli_affected_rows($link);
}

?>