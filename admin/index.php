<?php 
    
        require_once("../config/database.php");
        require_once("../models/product.php");

        $link = db_connect();
        if(isset($_GET['action']))
                $action = $_GET['action'];
        else
                $action = "";
        
        if($action == "add") {
                if(!empty($_POST))
                {
                  product_new($link,$_POST['name'],$_POST['sku'],$_POST['price'],$_POST['size'],$_POST['weight'],$_POST['size_f']);
                  header("Location: index.php");
                }
               

                include("../views/admin_product.php");
       
        }
        else if ($action == "delete")
        {        
                $id = $_GET['id'];
                $products = product_delete($link,$id);
                
                header("Location: index.php");
        }
        
        else
        {
                $products = product_all($link);
                include("../views/product_admin.php");
                
        }
?>